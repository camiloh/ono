#pragma once

#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <QObject>

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariantList>
#include <QVariantMap>


class IconSetModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList categories READ categories NOTIFY categoriesChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged);

public:
    explicit IconSetModel(QObject *parent = nullptr);
public:
    enum Roles {
        FileName = Qt::UserRole + 1,
        IconName,
        Icon,
        FullPath,
        Category,
        Scalable,
        Sizes,
        Type,
        Theme,
    };

    Q_INVOKABLE QVariantList inOtherThemes(const QString &iconName, int size);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QString key(int role) const;

    void add(const QFileInfo &info, const QString &cat);
    void remove(const QString &iconFile);

    QStringList categories() const;

    bool loading();

    void load();

    Q_INVOKABLE void output(const QString &text);

    Q_INVOKABLE void openContainingFolder(const QString &filename);

Q_SIGNALS:
    void categoriesChanged();
    void loadingChanged();

private:
    QHash<int, QByteArray> m_roleNames;

    QStringList m_icons;
    QStringList m_categories;
    QHash<QString, QVariantMap> m_data;
    QHash<QString, QString> m_categoryTranslations;

    bool m_loading;
    QString categoryFromPath(const QString &path);
};


class IconSetModelProxy : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(IconSetModel *model READ model FINAL CONSTANT)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged RESET resetFilter)
    Q_PROPERTY(QString category READ category WRITE setCategory NOTIFY categoryChanged)
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)

public:
    explicit IconSetModelProxy(QObject *parent = nullptr);

    void setFilter(QString filter);
    QString filter() const;
    void resetFilter();

    void setCategory(const QString &category);
    QString category() const;

    void setCurrentIndex(int index);
    int currentIndex();

    IconSetModel *model() const;

Q_SIGNALS:
    void filterChanged(QString filter);
    void categoryChanged();
    void currentIndexChanged();


private:
    IconSetModel *m_model;
    QString m_filter;
    QString m_category;
    QModelIndex m_currentSourceIndex;


protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override final;

};



