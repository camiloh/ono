import QtQuick 2.15
import QtQml 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12

import Qt.labs.settings 1.0

import org.mauikit.controls 1.3 as Maui

import org.kde.ono 1.0 as Ono

Maui.ApplicationWindow
{
    id: root
    title: Maui.App.about.displayName

    Maui.Style.styleType: settings.styleType

    Settings
    {
        id: settings
        category: "General"
        property int  styleType : Maui.Style.Dark
        property int iconSize : 48
        property bool showLabels : false
    }

    Maui.SideBarView
    {

        anchors.fill: parent
        sideBar.content: Maui.Page
        {
            anchors.fill: parent
            padding: 0
            Maui.ListBrowser
            {
                anchors.fill: parent
                model: _model.model.categories
                delegate: Maui.ListDelegate
                {
                    label: modelData
                    width: ListView.view.width
                    onClicked: _model.category = modelData
                    isCurrentItem: _model.category === modelData
                }
            }

            headBar.leftContent: Switch
            {
                text: i18n("Dark")
                checked: Maui.Style.styleType === Maui.Style.Dark
                onToggled:
                {
                    if(Maui.Style.styleType === Maui.Style.Dark)
                    {
                        Maui.Style.styleType = Maui.Style.Light
                    }else
                    {
                          Maui.Style.styleType = Maui.Style.Dark
                    }
                }
            }
        }

        Maui.Page
        {
            anchors.fill: parent
            showCSDControls: true


            headBar.forceCenterMiddleContent: root.isWide
            headBar.middleContent: Maui.SearchField
            {
                Layout.fillWidth: true
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignHCenter

                onAccepted: _model.filter = text
                onCleared: _model.filter = undefined
            }

            Maui.GridBrowser
            {
                anchors.fill: parent
                model: Ono.IconSetModel
                {
                    id: _model
                }

                itemSize: settings.iconSize + 22 + 22

                delegate: Item
                {
                    width: GridView.view.cellWidth
                    height: GridView.view.cellHeight

                    Maui.GridBrowserDelegate
                    {
                        anchors.fill: parent
                        anchors.margins: Maui.Style.space.medium
                        iconSizeHint: settings.iconSize
                        iconSource: model.iconName
                        label1.text: model.iconName
                        labelsVisible: settings.showLabels
                        template.labelSizeHint: 22
                        tooltipText: model.fileName
                    }
                }
            }
        }
    }
}
